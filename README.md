[LibreNote](https://svink.bitbucket.io) est un projet de site web et de wikis entiérement basé sur [TiddlyWiki](http://tiddlywiki.com/).

Le site LibreNote est accessible à l'adresse https://svink.bitbucket.io. Il regroupe une collection de wikis sur les logiciels libres et des démos d'utilisation de TiddlyWiki.
 
### Les Wikis
* [LinuxNote](https://svink.bitbucket.io/linuxnote) - Un guide sur Chakra, KDE et les Logiciels Libres

* [TiddlyNote](https://svink.bitbucket.io/tiddlynote) - Une introduction à TiddlyWiki

* [GitNote](https://svink.bitbucket.io/gitnote) - Les bases de Git

### Les Démos
* [TiddlyOueb](https://svink.bitbucket.io/tiddlyoueb) - Créer un site Web avec TiddlyWiki

* [TiddlyBlog](https://svink.bitbucket.io/tiddlyblog) - Créer un blog avec TiddlyWiki

### Les Outils
* [LectureSeule](https://svink.bitbucket.io/lectureseule) - Un mode lecture seule simple pour TW
